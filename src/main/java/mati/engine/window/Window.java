package mati.engine.window;

import mati.engine.util.Listener;
import mati.engine.util.Notifier;
import mati.engine.util.QSize;
import mati.engine.util.Size;
import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWFramebufferSizeCallback;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import org.lwjgl.opengl.GL11;
import org.lwjgl.system.MemoryUtil;

import java.nio.ByteBuffer;
import java.util.logging.Logger;

import static org.lwjgl.glfw.GLFW.*;

public class Window {

	private static final int defaultWidth = 800;
	private static final int defaultHeight = 600;
	private static final String defaultTitle = "Hello Engine!";

	private long handle = MemoryUtil.NULL;
	private final ReadOnlySize windowSize = new ReadOnlySize(defaultWidth, defaultHeight);
	private final ReadOnlyQSize windowFrameSize = new ReadOnlyQSize();
	private final ReadOnlySize frameSize = new ReadOnlySize();

	private Notifier<Size> windowSizeNotifier = new Notifier<>();
	private Notifier<QSize> windowFrameSizeNotifier = new Notifier<>();
	private Notifier<Size> frameSizeNotifier = new Notifier<>();

	private String title = defaultTitle;

	public long getHandle() {
		return handle;
	}

	public void setErrorLogger(final Logger logger) {
		glfwSetErrorCallback(new GLFWErrorCallback() {
			@Override
			public void invoke(int error, long description) {
				logger.severe("GLFW error: " + error + " " + description);
			}
		});
	}

	public void setTitle(String title) {
		this.title = title;
		if (handle != MemoryUtil.NULL) {
			glfwSetWindowTitle(handle, title);
		}
	}

	public void setWindowSize(int width, int height) {
		windowSize.setSize(width, height);
		if (handle != MemoryUtil.NULL) {
			glfwSetWindowSize(handle, width, height);
		}
	}

	public void init() {
		if (glfwInit() != GL11.GL_TRUE) {
			throw new IllegalStateException("Unable to initialize GLFW");
		}
		glfwDefaultWindowHints(); // optional, the currentObject window hints are already the default
		glfwWindowHint(GLFW_VISIBLE, GL11.GL_FALSE); // the window will stay hidden after creation
		glfwWindowHint(GLFW_RESIZABLE, GL11.GL_TRUE); // the window will be resizable
		handle = glfwCreateWindow(windowSize.getWidth(), windowSize.getHeight(), title, MemoryUtil.NULL, MemoryUtil.NULL);
		if (handle == MemoryUtil.NULL) {
			throw new RuntimeException("Failed to createChild the GLFW window");
		}
		checkWindowSize();
		checkWindowFrameSize();
		checkFrameSize();
		glfwSetFramebufferSizeCallback(handle, new GLFWFramebufferSizeCallback() {
			@Override
			public void invoke(long window, int width, int height) {
				frameSize.setSize(width, height);
				frameSizeNotifier.notify(frameSize);
			}
		});
		glfwSetWindowSizeCallback(handle, new GLFWWindowSizeCallback() {
			@Override
			public void invoke(long window, int width, int height) {
				windowSize.setSize(width, height);
				windowSizeNotifier.notify(windowSize);
			}
		});

		glfwMakeContextCurrent(handle);
		glfwSwapInterval(1);
	}

	private void checkFrameSize() {
		ByteBuffer width = BufferUtils.createByteBuffer(4);
		ByteBuffer height = BufferUtils.createByteBuffer(4);
		glfwGetFramebufferSize(handle, width, height);
		frameSize.setWidth(width.getInt(0));
		frameSize.setHeight(height.getInt(0));
	}

	private void checkWindowSize() {
		ByteBuffer width = BufferUtils.createByteBuffer(4);
		ByteBuffer height = BufferUtils.createByteBuffer(4);
		glfwGetWindowSize(handle, width, height);
		windowSize.setWidth(width.getInt(0));
		windowSize.setHeight(height.getInt(0));
	}

	private void checkWindowFrameSize() {
		ByteBuffer left = BufferUtils.createByteBuffer(4);
		ByteBuffer top = BufferUtils.createByteBuffer(4);
		ByteBuffer right = BufferUtils.createByteBuffer(4);
		ByteBuffer bottom = BufferUtils.createByteBuffer(4);
		glfwGetWindowFrameSize(handle, left, top, right, bottom);
		windowFrameSize.set(left.getInt(0), right.getInt(0), bottom.getInt(0), top.getInt(0));
	}

	public void show() {
		glfwShowWindow(handle);
	}

	public void hide() {
		glfwHideWindow(handle);
	}

	public void swapBuffers() {
		glfwSwapBuffers(handle);
	}

	public boolean isClosing() {
		return glfwWindowShouldClose(handle) != GL11.GL_FALSE;
	}

	public void addWindowSizeChangeListener(Listener<Size> listener) {
		windowSizeNotifier.add(listener);
	}

	public void removeWindowSizechangeListener(Listener<Size> listener) {
		windowSizeNotifier.remove(listener);
	}

	public void addWindowFrameSizeChengeListener(Listener<QSize> listener) {
		windowFrameSizeNotifier.add(listener);
	}

	public void removeWindowFrameSizeChangeListener(Listener<QSize> listener) {
		windowFrameSizeNotifier.remove(listener);
	}

	public void addFrameSizeChangeListener(Listener<Size> listener) {
		frameSizeNotifier.add(listener);
	}

	public void removeFrameSizeChangeListener(Listener<Size> listener) {
		frameSizeNotifier.remove(listener);
	}

	public void close() {
		glfwSetWindowShouldClose(handle, GL11.GL_TRUE);
	}

	private static class ReadOnlySize extends Size {

		public ReadOnlySize() {
			super();
		}

		public ReadOnlySize(int width, int height) {
			super(width, height);
		}

		void setWidth(int width) {
			this.width = width;
		}

		void setHeight(int height) {
			this.height = height;
		}

		void setSize(int width, int height) {
			this.width = width;
			this.height = height;
		}
	}

	private static class ReadOnlyQSize extends QSize {

		void set(int left, int right, int bottom, int top) {
			super.right = right;
			super.bottom = bottom;
			super.top = top;
			super.left = left;
		}
	}

}
