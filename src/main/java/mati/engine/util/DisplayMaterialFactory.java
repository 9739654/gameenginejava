package mati.engine.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import mati.engine.scene.component.DisplayMaterial;
import mati.simplelogger.SimpleLogger;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static mati.engine.util.FunctionalJson.*;
import static mati.engine.util.FunctionalJson.Ext.*;

public class DisplayMaterialFactory {
	protected static final Logger log = SimpleLogger.getLogger(DisplayMaterialFactory.class);

	private DisplayMaterial currentMaterial;

	public List<DisplayMaterial> load(URL url) {
		switch (getExtension(url)) {
			case "json":
				return loadJson(url);

			case "yaml":
				return loadYaml(url);

			default:
				log.warning("cannot load " + url + " expectedn .json or .yaml extension");
				return new ArrayList<>(0);
		}
	}

	private List<DisplayMaterial> loadYaml(URL url) {
		Yaml yaml = new Yaml(new Constructor(DisplayMaterial.class));
		List<DisplayMaterial> list = new ArrayList<>();
		try {
			for (Object data : yaml.loadAll(url.openStream())) {
				list.add((DisplayMaterial) data);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return list;
	}

	private List<DisplayMaterial> loadJson(URL url) {
		final List<DisplayMaterial> list = new ArrayList<>();
		try (InputStream is = url.openStream()) {
			java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
			String jsonText = s.hasNext() ? s.next() : "";
			is.close();

			JsonArray materialArray = new JsonParser().parse(jsonText).getAsJsonArray();
			materialArray.forEach(element -> {
				JsonObject json = element.getAsJsonObject();
				currentMaterial = from(json);
				list.add(currentMaterial);
			});

		} catch (IOException e) {
			log.severe(e.getLocalizedMessage());
		}
		return list;
	}

	public DisplayMaterial from(JsonObject json) {
		DisplayMaterial material = new DisplayMaterial();
		material.name = string(json, "name");
		JsonArray jsonMappings = array(json, "mappings");
		jsonMappings.forEach(this::parseMappingElement);

		return material;
	}

	private void parseMappingElement(JsonElement mappingElement) {
		JsonObject mapping = FunctionalJson.object(mappingElement);
		if (FunctionalJson.hasAny(mapping, "extract", "container")) {
			parseExtractMapping(mapping);
		} else if (mapping.has("value")) {
			parseValueMapping(mapping);
		}
	}

	private void parseExtractMapping(JsonObject json) {
		DisplayMaterial.ExtractMapping mapping = new DisplayMaterial.ExtractMapping();
		mapping.name = string(json, "name");
		mapping.type = type(json, "type");
		mapping.container = type(json, "container");
		mapping.fields = fields(strings(array(json, "fields")), Ext::declaredField, mapping.container);
		currentMaterial.extractMappings.put(mapping.name, mapping);
	}

	private void parseValueMapping(JsonObject json) {
		DisplayMaterial.ValueMapping mapping = new DisplayMaterial.ValueMapping();
		mapping.name = string(json, "name");
		mapping.type = type(json, "type");

		if (json.get("value").isJsonArray()) {
			mapping.value = Ext.instantiate(mapping.type, array(json, "value"));
		} else {
			mapping.value = Ext.instantiate(element(json, "value"));
		}

	}

	private static String getExtension(URL url) {
		String file = url.getFile();
		int dot = file.lastIndexOf('.');
		return file.substring(dot + 1).toLowerCase();
	}

}
