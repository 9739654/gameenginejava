package mati.engine.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

import static java.lang.String.format;

@SuppressWarnings("unused")
public class FunctionalJson {

	private static final String objectHasNoAttribute = "The given object %s has not attribute \"%s\"";
	private static final String typeNotFound = "There is no sych type \"%s\"";
	private static final String elementNotOfType = "The element \"%s\" is not of \"%s\" type";
	private static final String declaredFieldNotFound = "The class \"%s\" has no declared field \"%s\"";
	private static final String fieldNotFound = "The class \"%s\" has no field \"%s\"";
	private static final String cannotInstantiate = "Can not instantiate \"%s\"";

	public static boolean has(JsonObject object, String attribute) {
		return object.has(attribute);
	}

	public static boolean hasAny(JsonObject object, String... attributes) {
		for (String attribute : attributes) {
			if (has(object, attribute)) {
				return true;
			}
		}
		return false;
	}

	public static boolean hasAll(JsonObject object, String... attributes) {
		for (String attribute : attributes) {
			if (!has(object, attribute)) {
				return false;
			}
		}
		return true;
	}

	public static void doWith(Consumer<JsonObject> consumer, JsonObject object) {
		consumer.accept(object);
	}

	public static JsonElement element(JsonObject object, String attribute) {
		JsonElement jsonElement = object.get(attribute);
		if (jsonElement == null) {
			throw new RuntimeException(format(objectHasNoAttribute, object, attribute));
		}
		return jsonElement;
	}

	public static JsonObject object(JsonElement element) {
		JsonObject jsonObject;
		try {
			jsonObject = element.getAsJsonObject();
		} catch (IllegalStateException ex) {
			throw new RuntimeException(format(elementNotOfType, element, "JsonObject"));
		}
		return jsonObject;
	}

	public static String string(JsonObject object, String attribute) {
		JsonElement element = element(object, attribute);
		return element.getAsString();
	}

	/**
	 * Returns the Class object associated with the class or interface with the given string name.
	 * @param name name of Class
	 * @param <T> type of Class
	 * @return Class
	 */
	@SuppressWarnings("unchecked")
	public static <T> Class<T> type(String name) {
		Class<T> result;
		if (isPrimitive(name)) {
			result = primitiveType(name);
		} else {
			result = referenceType(name);
		}
		return result;
	}

	public static boolean isPrimitive(String name) {
		final String[] primitives = new String[] { "byte", "short", "int", "long", "float", "double", "boolean", "char" };
		for (String primitive : primitives) {
			if (primitive.equals(name)) {
				return true;
			}
		}
		return false;
	}

	public static <T> Class<T> primitiveType(String name) {
		try {
			switch (name) {
				case "byte":
					return (Class<T>) Byte.class;
				case "short":
					return (Class<T>) Short.class;
				case "int":
					return (Class<T>) Integer.class;
				case "long":
					return (Class<T>) Long.class;
				case "float":
					return (Class<T>) Float.class;
				case "double":
					return (Class<T>) Double.class;
				case "boolean":
					return (Class<T>) Boolean.class;
				case "char":
					return (Class<T>) Character.class;
			}
		} catch (Exception ex) {
			throw new RuntimeException(format(typeNotFound, name), ex);
		}
		throw new RuntimeException(format(typeNotFound, name));
	}

	public static <T> Class<T> referenceType(String name) {
		Class<T> result;
		try {
			result = (Class<T>) Class.forName(name);
		} catch (Exception e) {
			throw new RuntimeException(format(typeNotFound, name), e);
		}
		return result;
	}

	public static <T> Class<T> type(JsonObject object, String attribute) {
		String className = string(object, attribute);
		return type(className);
	}

	/**
	 * Tries to return the {@link JsonElement element} as String.
	 * @param element element to convert
	 * @return converted element
	 */
	private static String string(JsonElement element) {
		String value;
		try {
			value = element.getAsString();
		} catch (ClassCastException | IllegalStateException ex) {
			throw new RuntimeException(format(elementNotOfType, element, "String"), ex);
		}
		return value;
	}

	/**
	 * Tries to return the {@code attribute} of {@code object} as {@link com.google.gson.JsonArray JsonArray}.
	 * @param object object of which attribute to return
	 * @param attribute attribute to convert
	 * @return converted attribute
	 */
	public static JsonArray array(JsonObject object, String attribute) {
		JsonElement element = element(object, attribute);
		JsonArray array;
		try {
			array = element.getAsJsonArray();
		} catch (IllegalStateException ex) {
			throw new RuntimeException(format(elementNotOfType, attribute, "Array"), ex);
		}
		return array;
	}

	public static <T> ArrayList<String> strings(ArrayList<T> array, Function<T, String> function) {
		ArrayList<String> strings = new ArrayList<>(array.size());
		array.forEach(item -> strings.add(function.apply(item)));
		return strings;
	}

	public static <T> ArrayList<T> array(JsonArray array, Function<JsonElement, T> function) {
		ArrayList<T> elements =  new ArrayList<>(array.size());
		array.forEach(jsonElement -> elements.add(function.apply(jsonElement)));
		return elements;
	}

	public static ArrayList<String> strings(JsonArray array, Function<JsonElement, String> function) {
		return array(array, function);
	}

	public static ArrayList<String> strings(JsonArray array) {
		return strings(array, JsonElement::getAsString);
	}

	public static class Ext {

		public static <T> Field[] fields(List<String> strings, BiFunction<Class<T>, String, Field> function, Class<T> clazz) {
			Field[] fields = new Field[strings.size()];
			for (int i = 0; i < fields.length; i++) {
				fields[i] = function.apply(clazz, strings.get(i));
			}
			return fields;
		}

		public static <T> Field declaredField(Class<T> clazz, String name) {
			Field field;
			try {
				field = clazz.getDeclaredField(name);
			} catch (NoSuchFieldException ex) {
				throw new RuntimeException(format(declaredFieldNotFound, clazz.getName(), name), ex);
			}
			return field;
		}

		public static <T> Field field(Class<T> clazz, String name) {
			Field field;
			try {
				field = clazz.getField(name);
			} catch (NoSuchFieldException ex) {
				throw new RuntimeException(format(fieldNotFound, clazz.getName(), name), ex);
			}
			return field;
		}

		public static Integer tryInt(String string) {
			return tryFunction(string, Integer::parseInt);
		}

		public static Long tryLong(String string) {
			return tryFunction(string, Long::parseLong);
		}

		public static Float tryFloat(String string) {
			return tryFunction(string, Float::parseFloat);
		}

		public static Double tryDouble(String string) {
			return tryFunction(string, Double::parseDouble);
		}

		public static Character tryCharacter(String string) {
			if (string.length() == 1) {
				return string.charAt(0);
			} else {
				return null;
			}
		}

		public static Byte tryByte(String string) {
			return tryFunction(string, Byte::decode);
		}

		public static <S, T> T tryFunction(S input, Function<S, T> function) {
			try {
				return function.apply(input);
			} catch (Exception ex) {
				return null;
			}
		}

		@SafeVarargs
		public static <S> Object tryFunctions(S input, Function<S, Object>... functions) {
			for (Function<S, Object> function : functions) {
				Object value = function.apply(input);
				if (value != null) {
					return value;
				}
			}
			return null;
		}

		public static <T> T instantiate(JsonElement element) {
			if (element.isJsonPrimitive()) {
				try {
					@SuppressWarnings("unchecked")
					T instance = (T) Ext.tryFunctions(element.toString(), Ext::tryInt, Ext::tryLong, Ext::tryFloat, Ext::tryDouble, Ext::tryByte);
					if (instance != null) {
						return instance;
					}
				} catch (Exception e) {
					throw new RuntimeException(format(cannotInstantiate, element), e);
				}
			}
			throw new RuntimeException(format(cannotInstantiate, element));
		}

		public static <T> T instantiate(Class<T> type, JsonArray argArray) {
			ArrayList<ConstructorParam<?>> params = constructorParams(argArray);
			Class<?>[] argTypes = constructorArgTypes(params);
			Object[] args = constructorArgs(params);
			try {
				Constructor<T> constructor = type.getConstructor(argTypes);
				return constructor.newInstance(args);
			} catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
				throw new RuntimeException(format(cannotInstantiate, type.getName()), e);
			}
		}

		public static Class<?>[] constructorArgTypes(ArrayList<ConstructorParam<?>> constructorParams) {
			Class<?>[] types = new Class[constructorParams.size()];
			for (int i = 0; i < types.length; i++) {
				types[i] = constructorParams.get(i).type;
			}
			return types;
		}

		public static Object[] constructorArgs(ArrayList<ConstructorParam<?>> constructorParams) {
			Object[] args = new Object[constructorParams.size()];
			for (int i = 0; i < args.length; i++) {
				args[i] = constructorParams.get(i).arg;
			}
			return args;
		}

		public static ArrayList<ConstructorParam<?>> constructorParams(JsonArray array) {
			return array(array, Ext::constructorParam);
		}

		public static <T> ConstructorParam<T> constructorParam(JsonElement element) {
			return constructorParam(object(element));
		}

		public static <T> ConstructorParam<T> constructorParam(JsonObject object) {
			Class<T> type = type(string(object, "type"));
			T value = instantiate(element(object, "value"));
			return ConstructorParam.from(type, value);
		}

		private static class ConstructorParam<T> {
			public Class<T> type;
			public T arg;

			public ConstructorParam() {}

			public ConstructorParam(Class<T> type, T arg) {
				this.type = type;
				this.arg = arg;
			}

			public static <T> ConstructorParam<T>from(Class<T> type, T arg) {
				return new ConstructorParam<>(type, arg);
			}
		}

	}
}
