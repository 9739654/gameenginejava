package mati.engine.util.buffer;

import mati.engine.util.Index;
import org.lwjgl.BufferUtils;

import java.util.Collection;

public class IndexBufferObject extends BufferObject<Index> {

	private static final int dataSize = Index.BYTES;

	public IndexBufferObject(int maxSize) {
		super(Index.class, BufferUtils.createByteBuffer(maxSize * dataSize));
	}

	@Override
	public int getDataSize() {
		return dataSize;
	}

	@Override
	public int getFieldSize() {
		return 0;
	}

	@Override
	public int getFieldCount() {
		return 0;
	}

	@Override
	public void put(Index data) {
		byteBuffer.putInt(data.value);
	}

	@Override
	public void put(Index[] data) {
		for (Index index : data) {
			byteBuffer.putInt(index.value);
		}
	}

	@Override
	public void put(Collection<Index> data) {
		for (Index index : data) {
			byteBuffer.putInt(index.value);
		}
	}

	@Override
	public Index get() {
		return new Index(byteBuffer.getInt());
	}

	public int getInt() {
		return byteBuffer.getInt();
	}

	@Override
	public void get(Index out) {

	}
}
