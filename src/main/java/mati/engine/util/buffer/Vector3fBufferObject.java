package mati.engine.util.buffer;

import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

public class Vector3fBufferObject extends BufferObject<Vector3f> {

	private static final int fieldSize = Float.BYTES;
	private static final int fieldCount = 3;
	private static final int vertexSize = fieldCount * Float.BYTES;

	public Vector3fBufferObject(int maxVertices) {
		super(Vector3f.class, BufferUtils.createByteBuffer(maxVertices * vertexSize));

	}

	@Override
	public int getDataSize() {
		return vertexSize;
	}

	@Override
	public int getFieldSize() {
		return fieldSize;
	}

	@Override
	public int getFieldCount() {
		return fieldCount;
	}

	@Override
	public void put(Vector3f data) {
		byteBuffer.putFloat(data.x).putFloat(data.y).putFloat(data.z);
	}

	@Override
	public Vector3f get() {
		float x = byteBuffer.get();
		float y = byteBuffer.get();
		float z = byteBuffer.get();
		return new Vector3f(x, y, z);
	}

	@Override
	public void get(Vector3f out) {
		out.x = byteBuffer.get();
		out.y = byteBuffer.get();
		out.z = byteBuffer.get();
	}

}
