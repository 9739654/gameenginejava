package mati.engine.util.buffer;

import org.lwjgl.BufferUtils;

import java.nio.ByteBuffer;
import java.util.Collection;

/**
 *
 * @param <T> type of data stored
 */
public abstract class BufferObject<T> {

	protected Class<T> clazz;
	protected final ByteBuffer byteBuffer;

	public BufferObject(Class<T> clazz, ByteBuffer buffer) {
		this.clazz = clazz;
		byteBuffer = buffer;
	}

	public abstract int getDataSize();

	public abstract int getFieldSize();

	public abstract int getFieldCount();

	public abstract void put(T data);

	public abstract void put(T[] data);

	public abstract void put(Collection<T> data);

	public abstract T get();

	public abstract void get(T out);

	public void setPosition(int position) {
		byteBuffer.position(position * getDataSize());
	}

	public int getPosition() {
		return byteBuffer.position() / getDataSize();
	}

}
