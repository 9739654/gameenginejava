package mati.engine.util;

public class ColorRgba extends Color {

	public float a;

	public ColorRgba(float r, float g, float b) {
		this(r, g, b, 1);
	}

	public ColorRgba(float r, float g, float b, float a) {
		super(r, g, b);
		this.a = a;
	}

}
