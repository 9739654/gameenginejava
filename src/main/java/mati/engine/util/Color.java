package mati.engine.util;

import java.nio.ByteBuffer;

public class Color {

	public static final Color black = new Color(0,0,0);
	public static final Color red = new Color(1,0,0);
	public static final Color green = new Color(0,1,0);
	public static final Color blue = new Color(0,0,1);
	public static final Color white = new Color(1,1,1);

	public float r;
	public float g;
	public float b;

	public Color() {
		super();
	}

	public Color(float gray) {
		this(gray, gray, gray);
	}

	public Color(float r, float g, float b) {
		this.r = r;
		this.g = g;
		this.b = b;
	}

	public Color(Color other) {
		this.r = other.r;
		this.g = other.r;
		this.b = other.b;
	}

	public Color(ByteBuffer buffer) {
		this(0, buffer);
	}

	public Color(int index, ByteBuffer buffer) {
		r = buffer.getInt(index + 0);
		g = buffer.getInt(index + 1);
		b = buffer.getInt(index + 2);
	}

}
