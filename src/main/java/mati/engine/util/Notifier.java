package mati.engine.util;

import java.util.ArrayList;
import java.util.List;

public class Notifier<T> {
	private List<Listener<T>> listeners = new ArrayList<>(2);

	public void add(Listener<T> listener) {
		listeners.add(listener);
	}

	public void remove(Listener<T> listener) {
		while (listeners.remove(listener));
	}

	public void notify(T data) {
		listeners.forEach(l -> l.accept(data));
	}
}
