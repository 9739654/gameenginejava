package mati.engine.util;

public class QSize {
	protected int left;
	protected int right;
	protected int bottom;
	protected int top;

	public QSize() {}

	public QSize(int left, int right, int bottom, int top) {
		this.left = left;
		this.right = right;
		this.bottom = bottom;
		this.top = top;
	}

	public int getLeft() {
		return left;
	}

	public int getRight() {
		return right;
	}

	public int getBottom() {
		return bottom;
	}

	public int getTop() {
		return top;
	}
}
