package mati.engine.util;

public class Size {
	protected int width;
	protected int height;

	public Size() {}

	public Size(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

}
