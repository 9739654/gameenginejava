package mati.engine.util;

import java.util.function.Consumer;

@FunctionalInterface
public interface Listener<T> extends Consumer<T> {}
