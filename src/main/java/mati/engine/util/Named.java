package mati.engine.util;

public interface Named {
	String getName();
}
