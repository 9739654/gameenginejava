package mati.engine.util;

import java.util.TreeMap;

public class NameMap<T extends Named> extends TreeMap<String, T> {
	public void put(T item) {
		super.put(item.getName(), item);
	}
}
