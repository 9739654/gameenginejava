package mati.engine.util;

@FunctionalInterface
public interface Action {
	void process();
}
