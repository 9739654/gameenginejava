package mati.engine.util;

public class Index {

	public static final int BYTES = Integer.BYTES;

	public int value;

	public Index() {}

	public Index(int value) {
		this.value = value;
	}

	public Index(Integer integer) {
		this.value = integer.intValue();
	}

	public void inc() {
		inc(0);
	}

	public void inc(int diff) {
		value += diff;
	}

	public void dec() {
		dec(0);
	}

	public void dec(int diff) {
		value -= diff;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int newValue) {
		this.value = newValue;
	}
}
