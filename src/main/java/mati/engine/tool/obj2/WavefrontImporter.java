package mati.engine.tool.obj2;

import mati.engine.scene.GameObject;
import mati.engine.tool.obj.Material;
import mati.engine.tool.obj.ObjFile;
import mati.engine.util.buffer.IndexBufferObject;
import mati.simplelogger.SimpleLogger;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

public class WavefrontImporter {
	protected static final Logger log = SimpleLogger.getLogger(WavefrontImporter.class);

	private URL directory;
	private BufferedReader inFile;

	private IndexBufferObject indexBufferObject;

	public GameObject from(URL input) throws IOException {
		String path = input.getPath();
		String fileName = input.getFile();
		directory = new URL(path);
		log.info("Processing file: " + url);
		log.info("Working directory: " + directory);
		InputStream format = url.openStream();
		inFile = new BufferedReader(new InputStreamReader(format));
		String in;

		while ((in = inFile.readLine()) != null) {
			processLine(in);
		}
		format.close();
		file.optimize();
		ObjFile tmp = file;
		nullAll();
		return tmp;
	}


}
