package mati.engine.tool.x3d;

import mati.engine.asset.Geometry;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Iterator;

public class X3dParser {

	private Node current;

	public Node from(URL url) throws XMLStreamException, IOException {
		XMLInputFactory factory = XMLInputFactory.newInstance();
		try (InputStream input = url.openStream()) {
			XMLEventReader reader = factory.createXMLEventReader(input);

			current = Node.createRoot();

			while (reader.hasNext()) {
				XMLEvent event = reader.nextEvent();

				switch (event.getEventType()) {
					case XMLEvent.START_ELEMENT:
						startElement(event.asStartElement());
						break;

					case XMLEvent.END_ELEMENT:
						current = current.parent();
						break;
				}
			}
		}
		return current;
	}

	private void startElement(StartElement element) {
		String name = element.getName().getLocalPart();

		current = current.createChild(name);
		Iterator<Attribute> it = element.getAttributes();
		while (it.hasNext()) {
			Attribute a = it.next();
			current.set(a.getName().getLocalPart(), a.getValue());
		}
	}

	public Geometry from() {

		return null;
	}

}
