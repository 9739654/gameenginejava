package mati.engine.tool.x3d;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.*;

/**
 * @author Mateusz Paluch
 */
public class Node {
	private Node parent;
	private StringProperty name;
	private Map<String, String> attributes;
	private List<Node> nodes;

	{
		name = new SimpleStringProperty();
		attributes = new HashMap<>();
		nodes = new ArrayList<>();
	}

	public static final Node createRoot() {
		return new Node().name("root");
	}

	/**
	 * Sets the name of this Transform
	 * @param name New name
	 * @return this
	 */
	@NotNull
	public Node name(String name) {
		this.name.set(name);
		return this;
	}

	public String name() {
		return name.get();
	}

	/**
	 * Returns the value of the given attribute or null.
	 * @param name The name of the attribute
	 * @return The value of the attribute or null
	 */
	public String attrib(String name) {
		return attributes.get(name);
	}

	public Set<Map.Entry<String, String>> attribs() {
		return attributes.entrySet();
	}

	/**
	 * Associates the specified value with the specified name in the list of attributes (optional operation). If this previously contained a mapping for the name, the old value is replaced by the specified value.
	 *
	 * @param name name with which the specified value is to be associated
	 * @param value value to be associated with the specified name
	 * @return this
	 */
	@NotNull
	public Node set(String name, String value) {
		attributes.put(name, value);
		return this;
	}

	/**
	 * Returns the child of this with the specified name; not recursive.
	 * @param name The name of the child to search for
	 * @return
	 */
	public List<Node> childrenByTag(String name) {
		List<Node> result = new ArrayList<>();
		for (Node n : nodes) {
			if (n.name.equals(name)) {
				result.add(n);
			}
		}
		return result;
	}

	/**
	 * Searches for children with given name recursively.
	 * @param name
	 * @return List of found children
	 */
	@NotNull
	public List<Node> findChildren(String name) {
		List<Node> result = new ArrayList<>();
		findChildren(name, result);
		return result;
	}

	private void findChildren(String name, List<Node> result) {
		result.addAll(childrenByTag(name));
		nodes.forEach(node -> node.findChildren(name, result));
	}

	/**
	 *
	 * @param name
	 * @return
	 */
	public Node findAnyChild(String name) {
		for (Node n : nodes) {
			Node ret = n.findAny(name);
			if (ret != null) {
				return ret;
			}
		}
		return null;
	}

	public Node findAny(String name) {
		if (this.name.get().equals(name)) {
			return this;
		}
		for (Node n : nodes) {
			Node ret = n.findAny(name);
			if (ret != null) {
				return ret;
			}
		}
		return null;
	}

	/**
	 * Returns all children.
	 * @return
	 */
	public List<Node> children() {
		return nodes;
	}

	/**
	 * Creates new child and attachese it to this. Returns the new child
	 * @param name
	 * @return new child
	 */
	public Node createChild(String name) {
		Node child = new Node().name(name);
		attach(child);
		return child;
	}

	/**
	 *  Associates the given Transform with this Transform.
	 * @param child The child Transform
	 * @return this
	 */
	@NotNull
	public Node attach(@NotNull Node child) {
		nodes.add(child);
		child.parent = this;
		return this;
	}

	/**
	 *
	 * @param childName
	 * @return the removed item or null
	 */
	public List<Node> detach(String childName) {
		List<Node> result = new ArrayList<>();
		Iterator<Node> it = nodes.iterator();
		while (it.hasNext()) {
			Node n = it.next();
			if (n.name.equals(childName)) {
				result.add(n);
				it.remove();
			}
		}
		return result;
	}

	@Nullable
	public Node parent() {
		return parent;
	}

	@Override
	public String toString() {
		String s = name.get() + ":";
		for (Map.Entry<String, String> e : attributes.entrySet()) {
			s += " " + e.getKey() + "=\"" + e.getValue() + '"';
		}
		return s;
	}
}
