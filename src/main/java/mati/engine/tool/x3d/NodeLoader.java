package mati.engine.tool.x3d;

import mati.engine.asset.Geometry;
import mati.engine.scene.GameObject;
import mati.engine.scene.component.DisplayMaterial;
import mati.engine.scene.component.DisplayMesh;
import mati.engine.util.Index;
import mati.engine.util.buffer.IndexBufferObject;
import mati.engine.util.buffer.Vector3fBufferObject;
import mati.simplelogger.SimpleLogger;
import org.joml.AxisAngle4f;
import org.joml.Matrix4f;
import org.joml.Vector3f;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

class Transform {
	public Vector3f translation;
	public Vector3f scale;
	public AxisAngle4f rotation;

	public Matrix4f toMatrix() {
		//TODO build matrix
		return null;
	}
}

class Group {
	public String name;
}

class Appearance {

}

class Material {
	public String name;
	public Vector3f diffuse;
	public Vector3f specular;
	public Vector3f emissive;
	public float ambient;
	public float shininess;
	public float transparency;
}

abstract class IndexedSet {}

class IndexedTriangleSet extends IndexedSet {
	public boolean solid;
	public boolean normalPerVertex;
	public int highestIndex;
	public IndexBufferObject ibo;
	public Vector3fBufferObject vbo;
}

public class NodeLoader {
	protected static final Logger log = SimpleLogger.getLogger(NodeLoader.class);
	private static final Field[] vecotr3fFields = new Field[3];
	private static final Field[] indexFields = new Field[1];

	static {
		try {
			vecotr3fFields[0] = Vector3f.class.getDeclaredField("x");
			vecotr3fFields[1] = Vector3f.class.getDeclaredField("y");
			vecotr3fFields[2] = Vector3f.class.getDeclaredField("z");

			indexFields[0] = Index.class.getDeclaredField("value");
		} catch (NoSuchFieldException e) {
			log.severe(e.getLocalizedMessage());
		}
	}

	private Node node;
	private GameObject gameObject;

	private Map<String, Material> materials = new TreeMap<>();
	private Map<String, Group> groups = new TreeMap<>();
	private Map<String, Transform> transforms = new TreeMap<>();
	private Matrix4f transform;
	private Transform currentTransform;
	private Group currentGroup;
	private Material currentMaterial;
	private IndexedTriangleSet currentIndexedTriangleSet;

	private String filename;
	private String generator;

	private GameObject tree;

	private interface Action {
		void perform();
	}

	public GameObject from(Node node) {
		this.node = node;
		processNode();

		return gameObject;
	}

	private void processNode() {
		switch (node.name()) {
			case "X3D":
				processX3D();
				break;

			default:
				//TODO error: X3D expected
				break;
		}
	}

	private void levelUp() {
		node = node.parent();
	}

	private void processAny(String childName, Action action) {
		Node child = node.findAnyChild(childName);
		if (child != null) {
			processChild(child, action);
		}
	}

	private void processAll(String childName, Action action) {
		List<Node> children = node.findChildren(childName);
		children.forEach(child -> processChild(child, action));
	}

	private void processChild(Node child, Action action) {
		node = child;
		action.perform();
		levelUp();
	}

	private void processX3D() {
		processAny("head", this::processHead);
		processAny("Scene", this::processScene);
	}

	private void processHead() {
		processAll("meta", this::processMeta);
	}

	private void processMeta() {
		String name = node.attrib("name");
		String content = node.attrib("content");
		switch (name) {
			case "filename":
				filename = content;
				break;
			case "generator":
				generator = content;
				break;
		}
	}

	private void processScene() {
		tree = new GameObject("Scene");
		processAny("NavigationInfo", this::processNavigationInfo);
		processAny("Background", this::processBackground);
		processAny("Transform", this::processTransform);
	}

	private void processNavigationInfo() {
		//TODO What is this?
	}

	private void processBackground() {
		//TODO probably ignore Background node
	}

	private void processTransform() {
		currentTransform = getTransform();
		processTransformAttributes();
		processAll("Group", this::processGroup);
		processAll("PointLight", this::processPointLight);
		processAll("Viewpoint", this::processViewpoint);
		currentTransform = null;
	}

	private void processTransformAttributes() {
		String input = node.attrib("translation");
		//TODO check translation string
		currentTransform.translation = parseVector3f(input);
		input = node.attrib("scale");
		//TODO check scale string
		currentTransform.scale = parseVector3f(input);
		input = node.attrib("rotation");
		//TODO check rotation string
		currentTransform.rotation = parseAxisAngle4f(input);
		Matrix4f transformMatrix = currentTransform.toMatrix();
		this.transform.mul(transformMatrix);
	}

	private Transform getTransform() {
		String name = node.attrib("USE");
		if (name != null) {
			return transforms.get(name);
		} else {
			name = node.attrib("DEF");
			Transform transform = new Transform();
			if (name != null) {
				transforms.put(name, transform);
			}
			return transform;
		}
	}

	private void processGroup() {
		currentGroup = getGroup();
		tree = new GameObject(currentGroup.name, tree);
		processGroupAttributes();
		processAny("Shape", this::processShape);
		currentGroup = null;
	}

	private void processGroupAttributes() {

	}

	private Group getGroup() {
		String name = node.attrib("USE");
		if (name != null) {
			return groups.get(name);
		} else {
			name = node .attrib("DEF");
			Group group = new Group();
			group.name = name;
			if (name != null) {
				groups.put(name, group);
			}
			return group;
		}
	}

	private void processShape() {
		//TODO process other kinds of nodes
		processAny("Appearance", this::processAppearance);
		processAll("IndexedTriangleSet", this::processIndexedTriangleSet);

		convertMaterial();
		convertGeometry();
	}

	private void convertMaterial() {
		DisplayMaterial material = new DisplayMaterial();
		material.set("diffuse", currentMaterial.diffuse);
		material.set("specular", currentMaterial.specular);
		material.set("emissiveColor", currentMaterial.emissive);
		material.set("ambientIntensity", currentMaterial.ambient);
		material.set("shininess", currentMaterial.shininess);
		material.set("transparency", currentMaterial.transparency);
		currentMaterial = null;

		material.set("point", Vector3f.class, float.class, vecotr3fFields);
		material.set("index", Index.class, int.class, indexFields);
		tree.attach(material);
	}

	private void convertGeometry() {
		Geometry geometry = new Geometry();
		geometry.vbo = currentIndexedTriangleSet.vbo;
		geometry.ibo = currentIndexedTriangleSet.ibo;
		geometry.solid = currentIndexedTriangleSet.solid;
		geometry.verticesPerFace = 3;
		DisplayMesh mesh = new DisplayMesh();
		mesh.geometry = geometry;
		tree.attach(mesh);
		currentIndexedTriangleSet = null;
	}

	private void processAppearance() {
		//TODO other kinds of nodes?
		processAny("Material", this::processMaterial);
	}

	private void processMaterial() {
		currentMaterial = getMaterial();
		processMaterialAttributes();
		currentMaterial = null;
	}

	private Material getMaterial() {
		String name = node.attrib("USE");
		if (name != null) {
			return materials.get(name);
		} else {
			name = node.attrib("DEF");
			Material material = new Material();
			material.name = name;
			if (name != null) {
				materials.put(name, material);
			}
			return material;
		}
	}

	private void processMaterialAttributes() {
		currentMaterial.diffuse = parseVector3f(node.attrib("diffuseColor"));
		currentMaterial.specular = parseVector3f(node.attrib("specularColor"));
		currentMaterial.emissive = parseVector3f(node.attrib("emissiveColor"));
		currentMaterial.ambient = Float.parseFloat(node.attrib("ambientIntensity"));
		currentMaterial.shininess = Float.parseFloat(node.attrib("shininess"));
		currentMaterial.transparency = Float.parseFloat(node.attrib("transparency"));
	}

	private void processIndexedTriangleSet() {
		processIndexedTriangleSetAttributes();
		//TODO check if "any" or "all":
		processAll("Coordinate", this::processCoordinate);
	}

	private void processIndexedTriangleSetAttributes() {
		currentIndexedTriangleSet = new IndexedTriangleSet();
		currentIndexedTriangleSet.solid = Boolean.parseBoolean(node.attrib("solid"));
		currentIndexedTriangleSet.normalPerVertex = Boolean.parseBoolean(node.attrib("normalPerVertex"));
		processIndexes();
	}

	private void processIndexes() {
		String string = node.attrib("index");
		String[] strings = string.split(" ");
		IndexBufferObject ibo = new IndexBufferObject(strings.length);
		Index index = new Index();
		int highestIndex = -1;
		for (int i = 0; i < strings.length; i++) {
			index.value = Integer.parseInt(strings[i]);
			if (index.value > highestIndex) {
				highestIndex = index.value;
			}
			ibo.put(index);
		}
		currentIndexedTriangleSet.highestIndex = highestIndex;
		currentIndexedTriangleSet.ibo = ibo;
	}

	private void processCoordinate() {
		String string = node.attrib("point");
		String[] strings = string.split(" ");
		//TODO check strings.length % 3 == 0
		Vector3fBufferObject vbo = new Vector3fBufferObject(strings.length / 3);
		int i=0;
		Vector3f vec = new Vector3f();
		while (i<strings.length) {
			vec.x = Float.parseFloat(strings[i]);
			i += 1;
			vec.y = Float.parseFloat(strings[i]);
			i += 1;
			vec.z = Float.parseFloat(strings[i]);
			i += 1;
			vbo.put(vec);
		}
		currentIndexedTriangleSet.vbo = vbo;
	}

	private void processPointLight() {
		//TODO
	}

	private void processViewpoint() {
		//TODO
	}

	private static Vector3f parseVector3f(String input) {
		Vector3f vector = new Vector3f();
		String[] parts = input.split(" ");
		//TODO check parts' length
		vector.x = Float.parseFloat(parts[0]);
		vector.y = Float.parseFloat(parts[1]);
		vector.z = Float.parseFloat(parts[2]);
		return vector;
	}

	private static AxisAngle4f parseAxisAngle4f(String input) {
		AxisAngle4f rotation = new AxisAngle4f();
		String[] parts = input.split(" ");
		//TODO check parts' length
		rotation.x = Float.parseFloat(parts[0]);
		rotation.y = Float.parseFloat(parts[1]);
		rotation.z = Float.parseFloat(parts[2]);
		rotation.angle = Float.parseFloat(parts[3]);
		return rotation;
	}

}
