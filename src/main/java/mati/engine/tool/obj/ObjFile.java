package mati.engine.tool.obj;

import mati.engine.asset.Texture;
import mati.engine.util.ColorRgba;
import org.joml.Vector2f;
import org.joml.Vector3f;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;

interface Named {
	String getName();
}

class NameMap<T extends Named> extends TreeMap<String, T> {
	public void put(T item) {
		super.put(item.getName(), item);
	}

	@Override
	public T put(String key, T value) {
		throw new RuntimeException("#put(String, T) not safe");
	}
}

public class ObjFile implements Named {
	private String name;
	private NameMap<Object> objects = new NameMap<>();
	private Object global = new Object("  global");
	private Object current = global;

	{
		objects.put(global);
	}

	public ObjFile(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void useMaterial(Material current) {
		this.current.useMaterial(current);
	}

	public Object setObject(String objectName) {
		current = objects.get(objectName);
		if (current == null) {
			current = new Object(objectName);
			objects.put(current);
		}
		return current;
	}

	public Object getObject(String objectName) {
		return objects.get(objectName);
	}

	public Collection<Object> getObjects() {
		return objects.values();
	}

	public Object currentObject() {
		return current;
	}

	public void optimize() {
		objects.values().forEach(Object::optimize);
		Iterator<Map.Entry<String, Object>> iterator = objects.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<String, Object> next = iterator.next();
			if (next.getValue().isEmpty()) {
				iterator.remove();
			}
		}
	}
}

class Object implements Named {
	private String name;
	private NameMap<Group> groups = new NameMap<>();
	private Group global = new Group("  global");
	private Group current = global;

	{
		groups.put(global);
	}

	public Object(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	public void useMaterial(Material current) {
		this.current.useMaterial(current);
	}

	public Group setGroup(String groupName) {
		current = groups.get(groupName);
		if (current == null) {
			current = new Group(groupName);
			groups.put(current);
		}
		return current;
	}

	public Group getGroup(String groupName) {
		return groups.get(groupName);
	}

	public Collection<Group> getGroups() {
		return groups.values();
	}

	public Group currentGroup() {
		return current;
	}

	public void optimize() {
		Iterator<Map.Entry<String, Group>> iterator = groups.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<String, Group> next = iterator.next();
			if (next.getValue().isEmpty()) {
				iterator.remove();
			}
		}
	}

	public boolean isEmpty() {
		return groups.isEmpty();
	}

}

class Group implements Named {
	private String name;
	private Map<Material, List<Face>> materialFaces = new TreeMap<>();
	private Map<Material, List<Triangle>> materialTriangles = new TreeMap<>();
	private Map<Material, List<Quad>> materialQuads = new TreeMap<>();
	private List<Face> currentFaces;
	private List<Triangle> currentTriangles;
	private List<Quad> currentQuads;
	private Material activeMaterial = new DefaultMaterial();

	public Group(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	public void useMaterial(Material current) {
		this.activeMaterial = current;
		currentFaces = getCurrentFaces();
		currentTriangles = getCurrentTriangles();
		currentQuads = getCurrentQuads();
	}

	private List<Face> getCurrentFaces() {
		List<Face> faces = materialFaces.get(activeMaterial);
		if (faces == null) {
			faces = new ArrayList<>();
			materialFaces.put(activeMaterial, faces);
		}
		return faces;
	}

	private List<Triangle> getCurrentTriangles() {
		List<Triangle> triangles = materialTriangles.get(activeMaterial);
		if (triangles == null) {
			triangles = new ArrayList<>();
			materialTriangles.put(activeMaterial, triangles);
		}
		return triangles;
	}

	private List<Quad> getCurrentQuads() {
		List<Quad> quads = materialQuads.get(activeMaterial);
		if (quads == null) {
			quads = new ArrayList<>();
			materialQuads.put(activeMaterial, quads);
		}
		return quads;
	}

	public void add(Face face) {
		currentFaces.add(face);
	}

	public void add(Triangle triangle) {
		currentTriangles.add(triangle);
	}

	public void add(Quad quad) {
		currentQuads.add(quad);
	}

	public List<Face> getFaces() {
		return materialFaces
				.values()
				.stream()
				.flatMap(List::stream)
				.collect(Collectors.toList());
	}

	public List<Triangle> getTriangles() {
		return materialTriangles
				.values()
				.stream()
				.flatMap(List::stream)
				.collect(Collectors.toList());
	}

	public List<Quad> getQuads() {
		return materialQuads
				.values()
				.stream()
				.flatMap(List::stream)
				.collect(Collectors.toList());
	}

	public void forEachFaceMaterial(BiConsumer<Material, List<Face>> action) {
		materialFaces.forEach(action);
	}

	public void forEachTriangleMaterial(BiConsumer<Material, List<Triangle>> action) {
		materialTriangles.forEach(action);
	}

	public void forEachQuadMaterial(BiConsumer<Material, List<Quad>> action) {
		materialQuads.forEach(action);
	}

	public boolean isEmpty() {
		return materialFaces.isEmpty() && materialTriangles.isEmpty() && materialQuads.isEmpty();
	}
}

class Material implements Named {
	private String name;
	public ColorRgba ambient;
	public ColorRgba diffuse;
	public ColorRgba specular;
	public ColorRgba emissive;
	public float shininess;
	public TextureSet textures = new TextureSet();

	{
		ambient = new ColorRgba(.2f, .2f, .2f, 1);
		diffuse = new ColorRgba(.8f, .8f, .8f, 1);
		specular = new ColorRgba(1,1,1,1);
	}

	public Material(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}
}

class DefaultMaterial extends Material {
	public DefaultMaterial() {
		super("default");
		ambient = new ColorRgba(.2f, .2f, .2f, 1);
		diffuse = new ColorRgba(.8f, .8f, .8f, 1);
		specular = new ColorRgba(1,1,1,1);
	}
}

class TextureSet {
	public Texture textureAmbient;
	public Texture textureDiffuse;
	public Texture textureSpecular;
	public Texture textureShininess;
	public Texture textureAlpha;
	public Texture textureBump;
	public Texture textureDisplacement;
	public Texture textureStencilDecal;
}

abstract class Face {
	public final Vector3f[] vertices;
	public final Vector2f[] texCoords;
	public final Vector3f[] normals;

	protected Face(int vertexCount) {
		vertices = new Vector3f[vertexCount];
		texCoords = new Vector2f[vertexCount];
		normals = new Vector3f[vertexCount];
	}

	protected Face(Vector3f[] v, Vector2f[] texCoords, Vector3f[] normals) {
		this.vertices = v;
		this.texCoords = texCoords;
		this.normals = normals;
	}

	public int vertexCount() {
		return vertices.length;
	}
}

class Triangle extends Face {

	public Triangle() {
		super(3);
	}

	public Triangle(Vector3f a, Vector3f b, Vector3f c,
	                Vector2f ta, Vector2f tb, Vector2f tc,
	                Vector3f na, Vector3f nb, Vector3f nc) {
		super(new Vector3f[] {a, b, c}, new Vector2f[] {ta, tb, tc}, new Vector3f[] {na, nb, nc});
	}

}

class Quad extends Face {

	public Quad() {
		super(4);
	}

	public Quad(Vector3f a, Vector3f b, Vector3f c, Vector3f d,
	            Vector2f ta, Vector2f tb, Vector2f tc, Vector2f td,
	            Vector3f na, Vector3f nb, Vector3f nc, Vector3f nd) {
		super(new Vector3f[]{a,b,c,d}, new Vector2f[]{ta,tb,tc,td}, new Vector3f[]{na,nb,nc,nd});
	}
}
