package mati.engine.tool.obj;

import mati.engine.scene.GameObject;
import mati.engine.scene.component.DisplayMaterial;
import mati.engine.scene.component.DisplayMesh;
import mati.engine.util.buffer.Vector3fBufferObject;

import java.util.*;

public class ObjToVbo {
	private GameObjectConverter groupObject;
	private GameObject currentFile;
	private GameObject currentObject;
	private GameObject currentGroup;

	public GameObject from(ObjFile file) {
		currentFile = new GameObject(file.getName());
		file.getObjects().forEach(this::processObject);
		return resultAndCleanUp();
	}

	/**
	 * cleans up and returns the result
	 * @return
	 */
	private GameObject resultAndCleanUp() {
		groupObject = null;
		GameObject tmp = currentFile;
		currentFile = null;
		currentObject = null;
		currentGroup = null;
		return tmp;
	}

	private void processObject(Object object) {
		currentObject = new GameObject(object.getName());
		currentFile.attach(currentObject);
		object.getGroups().forEach(this::processGroup);
	}

	private void processGroup(Group group) {
		this.currentGroup = groupObject.from(group);
		currentObject.attach(currentGroup);
	}

}

class GameObjectConverter {
	private Group input;
	private GameObject groupObject;

	/**
	 * Maps material name to material data (material, triangles, quads, faces)
	 */
	private Map<String, MaterialData> materialDatas;

	{
		materialDatas = new TreeMap<>();
	}

	public GameObject from(Group group) {
		this.input = group;
		processInput();
		createOutput();
		return groupObject;
	}

	private void processInput() {
		input.forEachFaceMaterial(this::processFaces);
		input.forEachTriangleMaterial(this::processTriangles);
		input.forEachQuadMaterial(this::processQuads);
	}

	private void createOutput() {
		groupObject = new GameObject(input.getName());
		materialDatas.values().forEach(this::processMaterial);
	}

	private void processFaces(Material material, List<Face> faces) {
		MaterialData materialData = getMaterialData(material);
		materialData.addFaces(faces);
	}

	private void processTriangles(Material material, List<Triangle> triangles) {
		getMaterialData(material).addTriangles(triangles);
	}

	private void processQuads(Material material, List<Quad> quads) {
		getMaterialData(material).addQuads(quads);
	}

	private void processMaterial(MaterialData materialData) {
		GameObject materialObject = new GameObject(materialData.material.getName());
		//TODO
		DisplayMaterial material = new DisplayMaterial();
	}

	private MaterialData getMaterialData(Material material) {
		MaterialData materialData = materialDatas.get(material.getName());
		if (materialData == null) {
			materialData = new MaterialData();
			materialData.material = material;
			materialDatas.put(material.getName(), materialData);
		}
		return materialData;
	}

	private static class MaterialData {
		Material material;
		List<Triangle> triangles;
		List<Quad> quads;
		List<Face> faces;

		public void add(Collection<Face> newFaces, Collection<Triangle> newTriangles, Collection<Quad> newQuads) {
			addFaces(newFaces);
			addTriangles(newTriangles);
			addQuads(newQuads);
		}

		public void addFaces(Collection<Face> newFaces) {
			if (faces == null) {
				faces = new ArrayList<>(newFaces.size());
			}
			faces.addAll(newFaces);
		}

		public void addTriangles(Collection<Triangle> newTriangles) {
			if (triangles == null) {
				triangles = new ArrayList<>(newTriangles.size());
			}
			triangles.addAll(newTriangles);
		}

		public void addQuads(Collection<Quad> newQuads) {
			if (quads == null) {
				quads = new ArrayList<>(newQuads.size());
			}
			quads.addAll(newQuads);
		}
	}
}
