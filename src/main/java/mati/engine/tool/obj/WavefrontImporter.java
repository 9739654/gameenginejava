package mati.engine.tool.obj;

import mati.engine.asset.Texture;
import mati.engine.util.ColorRgba;
import org.joml.Vector2f;
import org.joml.Vector3f;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.logging.Logger;

import static com.sun.javafx.util.Utils.clamp;

public class WavefrontImporter {
	private static final Logger log = Logger.getLogger(WavefrontImporter.class.getSimpleName());

	private BufferedReader inFile;

	private List<Vector3f> vertexList = new ArrayList<>();
	private List<Vector2f> textureList = new ArrayList<>();
	private List<Vector3f> normalList = new ArrayList<>();

	/** Generated normals */
	private List<Vector3f> genNormalList = new ArrayList<>();

	/** Last 'material' flag in the file */
	private Material currentMaterial;

	/** Last 'File' name in the file */
	private String currentObjectName = null;

	/** Last 'Group' name in the file */
	private String currentGroupName = null;

	/** Default material group for groups without a material */
	private Material defaultMaterial;

	/** Maps material names to the actual material object * */
	private Map<String, Material> materials = new TreeMap<>();

	/** Maps Materials to their vertex usage * */
	private Map<Material, ArraySet> materialVertices = new HashMap<>();

	/** Reference to the renderer for creating RenderState objects **/
	//private Renderer renderer;

	private boolean generateMissingNormals = true;

	private URL directory;

	private ObjFile file;

	@SuppressWarnings("unused")
	public ObjFile convert(URL url) throws IOException {
		String path = url.getPath();
		String fileName = url.getFile();
		directory = new URL(path);
		log.info("Processing file: " + url);
		log.info("Working directory: " + directory);
		InputStream format = url.openStream();
		defaultMaterial = new Material("default");
		vertexList.clear();
		textureList.clear();
		normalList.clear();
		genNormalList.clear();
		materialVertices.clear();
		materials.clear();
		inFile = new BufferedReader(new InputStreamReader(format));
		String in;
		currentMaterial = defaultMaterial;
		materialVertices.put(defaultMaterial, new ArraySet());

		file = new ObjFile(fileName);

		while ((in = inFile.readLine()) != null) {
			processLine(in);
		}
		format.close();
		file.optimize();
		ObjFile tmp = file;
		nullAll();
		return tmp;
	}

	/**
	 * Nulls all to let the gc do its job.
	 *
	 * @throws IOException
	 */
	private void nullAll() throws IOException {
		vertexList.clear();
		textureList.clear();
		normalList.clear();
		genNormalList.clear();
		currentMaterial = null;
		materialVertices.clear();
		materials.clear();
		inFile.close();
		inFile = null;
		defaultMaterial = null;
		file = null;
	}

	/**
	 * Processes a line of text in the .obj file.
	 *
	 * @param s
	 *            The line of text in the file.
	 * @throws IOException
	 */
	private void processLine(String s) throws IOException {
		if (s == null || s.length() == 0) {
			return;
		}
		String[] parts = s.split("\\s+");
		parts = removeEmpty(parts);
		if (parts.length == 0) {
			return;
		}
		switch (parts[0]) {
			case "v":
				addVertex(parts);
				return;

			case "vt":
				addTexCoord(parts);
				return;

			case "vn":
				addNormal(parts);
				return;

			case "g":
				file.currentObject().setGroup(parts[1]);
				return;

			case "f":
				addFace(parts);
				return;

			case "mtllib":
				loadMaterialLibrary(parts);
				return;

			case "newmtl":
				newMaterial(parts);
				return;

			case "usemtl":
				currentMaterial = materials.get(parts[1]);
				file.useMaterial(currentMaterial);
				return;

			case "Ka": {
				float r = Float.parseFloat(parts[1]);
				float g = Float.parseFloat(parts[2]);
				float b = Float.parseFloat(parts[3]);
				ColorRgba c = new ColorRgba(r, g, b, 1);
				currentMaterial.ambient = c;
				return;
			}

			case "Kd": {
				float r = Float.parseFloat(parts[1]);
				float g = Float.parseFloat(parts[2]);
				float b = Float.parseFloat(parts[3]);
				ColorRgba c = new ColorRgba(r, g, b, 1);
				currentMaterial.diffuse = c;
				return;
			}

			case "Ks": {
				float r = Float.parseFloat(parts[1]);
				float g = Float.parseFloat(parts[2]);
				float b = Float.parseFloat(parts[3]);
				ColorRgba c = new ColorRgba(r, g, b, 1);
				currentMaterial.specular = c;
				return;
			}

			case "Ns": {
				float shine = Float.parseFloat(parts[1]);
				shine = clamp(0, shine, 128);
				currentMaterial.shininess = shine;
				return;
			}

			case "d": {
				float alpha = Float.parseFloat(parts[1]);
				currentMaterial.ambient.a *= alpha;
				currentMaterial.diffuse.a *= alpha;
				currentMaterial.specular.a *= alpha;
				if (alpha < 1.0) {
					//activeMaterial.createBlendState();
				}
				return;
			}

			case "map_d":
				//activeMaterial.createBlendState();
				return;

			case "map_Kd": {
				currentMaterial.textures.textureDiffuse = loadTexture(parts);
				return;
			}

			case "map_Ka": {
				currentMaterial.textures.textureAmbient = loadTexture(parts);
				return;
			}

			case "map_Ks": {
				currentMaterial.textures.textureSpecular = loadTexture(parts);
			}
			//TODO: from other textures

			case "o":
				file.setObject(parts[1]);
				return;

			default:
				if (parts[0].charAt(0) == '#') {
					return;
				} else {
					log.warning("Unknown line: " + s);
				}
		}
	}

	private Texture loadTexture(String[] params) throws MalformedURLException {
		List<String> options = Arrays.asList(Arrays.copyOfRange(params, 1, params.length));
		Iterator<String> it = options.iterator();
		while (it.hasNext()) {
			String option = it.next();
			it.remove();
			switch (option) {
				case "-blendu":
					break;
				case "-blendv":
					break;
				case "-boost":
					break;
				case "-mm":
					break;
				case "-o":
					break;
				case "-s":
					break;
				case "-t":
					break;
				case "-texres":
					break;
				case "-clamp":
					break;
				case "-bm":
					break;
				case "-imfchan":
					break;
				default:
					log.warning("Unknown texture option: " + option);
			}
		}
		log.info("Remaining options: " + options.toString());
		URL textureUrl = new URL(directory, params[params.length - 1]);
		return new Texture(textureUrl);
	}

	private String[] removeEmpty(String[] parts) {
		int notEmpty = 0;
		for (int i = 0; i < parts.length; i++) {
			if (!parts[i].equals(""))
				notEmpty++;
		}
		String[] result = new String[notEmpty];
		int index = 0;
		for (int i = 0; i < parts.length; i++) {
			if (!parts[i].equals("")) {
				result[index] = parts[i].trim();
				index += 1;
			}
		}
		return result;
	}

	private void newMaterial(String[] parts) {
		String materialName = parts[1];
		Material material = new Material(materialName);
		materials.put(materialName, material);
		materialVertices.put(material, new ArraySet());
		currentMaterial = material;
	}

	private void loadMaterialLibrary(String[] fileNames) throws IOException {
		fileNames = Arrays.copyOfRange(fileNames, 1, fileNames.length);
		for (String filename : fileNames) {
			processMaterialFile(new URL(directory, filename).openStream());
		}
	}

	private void processMaterialFile(InputStream inputStream) throws IOException {
		BufferedReader materialReader = new BufferedReader(new InputStreamReader(inputStream));
		String in;
		while ((in = materialReader.readLine()) != null) {
			processLine(in);
		}
	}

	private void addFace(String[] parts) {
		ArraySet thisMaterialVertices = materialVertices.get(currentMaterial);
		if (thisMaterialVertices.objectName == null) {
			if (currentObjectName != null) {
				thisMaterialVertices.objectName = currentObjectName;
			} else if (currentGroupName != null) {
				thisMaterialVertices.objectName = currentGroupName;
			}
		}
		IndexSet first = new IndexSet(parts[1]);
		int firstIndex = thisMaterialVertices.findSet(first);
		IndexSet second = new IndexSet(parts[2]);
		int secondIndex = thisMaterialVertices.findSet(second);
		for (int i = 3; i < parts.length; i++) {
			IndexSet third = new IndexSet();
			third.parseStringArray(parts[i]);
			int thirdIndex = thisMaterialVertices.findSet(third);
			thisMaterialVertices.indexes.add(firstIndex);
			thisMaterialVertices.indexes.add(secondIndex);
			thisMaterialVertices.indexes.add(thirdIndex);
			if (first.normalIndex == -1 || second.normalIndex == -1 || third.normalIndex == -1) {
				// Generate flat face normal.  TODO: Smoothed normals?
				Vector3f v = new Vector3f(vertexList.get(second.vertexIndex));
				Vector3f w = new Vector3f(vertexList.get(third.vertexIndex));
				v.sub(vertexList.get(first.vertexIndex));
				w.sub(vertexList.get(first.vertexIndex));
				v.cross(w);
				v.normalize();
				genNormalList.add(v);
				int genIndex = (-1 * (genNormalList.size() - 1)) - 2;
				if (first.normalIndex == -1) {
					first.normalIndex = genIndex;
				}
				if (second.normalIndex == -1) {
					second.normalIndex = genIndex;
				}
				if (third.normalIndex == -1) {
					third.normalIndex = genIndex;
				}
			}
			secondIndex = thirdIndex; // The second will be the same as the last third
		}
	}

	private void addNormal(String[] parts) {
		float x = Float.parseFloat(parts[1]);
		float y = Float.parseFloat(parts[2]);
		float z = Float.parseFloat(parts[3]);
		Vector3f normal = new Vector3f(x, y, z);
		normalList.add(normal);
	}

	private void addTexCoord(String[] parts) {
		float u = Float.parseFloat(parts[1]);
		float v = parts.length > 2 ? Float.parseFloat(parts[2]) : 0;
		textureList.add(new Vector2f(u, v));
	}

	private void addVertex(String[] parts) {
		float x = Float.parseFloat(parts[1]);
		float y = Float.parseFloat(parts[2]);
		float z = Float.parseFloat(parts[3]);
		vertexList.add(new Vector3f(x, y, z));
	}

	/**
	 * Stores the indexes of a vertex/texture/normal triplet setGroup that is to be
	 * indexed by the TriMesh.
	 */
	private class IndexSet {
		int vertexIndex, normalIndex, texCoordIndex;

		public IndexSet() {}

		public IndexSet(String parts) {
			parseStringArray(parts);
		}

		public void parseStringArray(String parts) {
			String[] triplet = parts.split("/");
			vertexIndex = Integer.parseInt(triplet[0]);
			if (vertexIndex < 0) {
				vertexIndex += vertexList.size();
			} else {
				vertexIndex -= 1;
			}

			if (triplet.length < 2 || triplet[1] == null || triplet[1].equals("")) {
				texCoordIndex = -1;
			} else {
				texCoordIndex = Integer.parseInt(triplet[1]);
				if (texCoordIndex < 0) {
					texCoordIndex += textureList.size();
				} else {
					texCoordIndex -= 1;  // obj starts at 1 not 0
				}
			}

			if (triplet.length != 3 || triplet[2] == null || triplet[2].equals("")) {
				normalIndex = -1;
			} else {
				normalIndex = Integer.parseInt(triplet[2]);
				if (normalIndex < 0) {
					normalIndex += normalList.size();
				} else {
					normalIndex -= 1;  // obj starts at 1 not 0
				}

			}
		}

		@Override
		public boolean equals(java.lang.Object obj) {
			if (!(obj instanceof IndexSet)) {
				return false;
			}

			IndexSet other = (IndexSet)obj;
			return other.normalIndex == this.normalIndex &&
					other.texCoordIndex == this.texCoordIndex &&
					other.vertexIndex == this.vertexIndex;
		}

		@Override
		public int hashCode() {
			int hash = 37;
			hash += 37 * hash + vertexIndex;
			hash += 37 * hash + normalIndex;
			hash += 37 * hash + texCoordIndex;
			return hash;
		}
	}

	/**
	 * An array of information that will become a renderable trimesh. Each
	 * material has it's own trimesh.
	 */
	private class ArraySet {
		private String objectName;
		private Set<IndexSet> sets = new LinkedHashSet<>();
		private Map<IndexSet, Integer> index = new HashMap<>();
		private List<Integer> indexes = new ArrayList<>();

		public int findSet(IndexSet v) {
			if (sets.contains(v)) {
				return index.get(v);
			}

			sets.add(v);
			index.put(v, sets.size() - 1);
			return sets.size() - 1;
		}
	}

	/**
	 * @return true if the loader will generate missing face normals (default is true)
	 */
	public boolean isGenerateMissingNormals() {
		return generateMissingNormals;
	}

	/**
	 * Set whether to generate missing face normals.
	 *
	 * @param generateMissingNormals
	 *            the generateMissingNormals to setGroup
	 */
	public void setGenerateMissingNormals(boolean generateMissingNormals) {
		this.generateMissingNormals = generateMissingNormals;
	}
}
