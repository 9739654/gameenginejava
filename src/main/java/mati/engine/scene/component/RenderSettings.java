package mati.engine.scene.component;

import mati.engine.scene.Component;
import mati.engine.scene.component.annotation.Dependency;
import mati.engine.scene.component.annotation.Inject;

import java.util.ArrayList;
import java.util.List;

@Dependency(DisplayMesh.class)
@Dependency(DisplayMaterial.class)
public class RenderSettings extends Component {

	@Inject
	private DisplayMaterial material;

	@Inject
	private DisplayMesh mesh;

	private List<RenderSettings> children = new ArrayList<>();

	public DisplayMaterial getMaterial() {
		return material;
	}

	public DisplayMesh getMesh() {
		return mesh;
	}

	@Override
	public void onStart() {
		super.onStart();
		mesh = getOwner().get(DisplayMesh.class);
		material = getOwner().get(DisplayMaterial.class);

		getOwner().getChildren().forEach(gameObject -> {
			RenderSettings rs = gameObject.get(RenderSettings.class);
			if (rs != null) {
				children.add(rs);
			}
		});
	}
}
