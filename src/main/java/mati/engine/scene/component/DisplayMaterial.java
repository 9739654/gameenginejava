package mati.engine.scene.component;

import mati.engine.scene.Component;
import mati.engine.util.NameMap;
import mati.engine.util.Named;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.TreeMap;

public class DisplayMaterial extends Component {

	public String name;
	public NameMap<ValueMapping> valueMappings = new NameMap<>();
	public NameMap<ExtractMapping> extractMappings = new NameMap<>();

	public DisplayMaterial set(String name, Object value) {
		valueMappings.put(new ValueMapping(value.getClass(), value));
		return this;
	}

	public <S, T> DisplayMaterial set(String name, Class<S> container, Class<T> type, Field... fields) {
		extractMappings.put(new ExtractMapping(name, container, type, fields));
		return this;
	}

	public static abstract class Mapping implements Named {
		public String name;

		public String getName() {
			return name;
		}
	}

	public static class ValueMapping<T> extends Mapping {
		public Class<T> type;
		public T value;

		public ValueMapping() {}

		public ValueMapping(Class<T> type, T value) {
			this.type = type;
			this.value = value;
		}
	}

	public static class ExtractMapping<S, T> extends Mapping {
		public Class<S> container;
		public Class<T> type;
		public Field[] fields;

		public ExtractMapping() {}

		public ExtractMapping(String name, Class<S> container, Class<T> type, Field[] fields) {
			this.name = name;
			this.container = container;
			this.type = type;
			this.fields = fields;
		}
	}

}
