package mati.engine.scene.component.annotation;

import mati.engine.scene.Component;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Repeatable(Dependencies.class)
public @interface Dependency {
	Class<? extends Component> value();
}
