package mati.engine.scene;

import com.sun.javafx.collections.ObservableMapWrapper;
import javafx.beans.Observable;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import mati.engine.scene.component.annotation.Inject;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Comparator;
import java.util.Objects;
import java.util.TreeMap;
import java.util.logging.Logger;

public class GameObject {
	private static final Logger log = Logger.getLogger(GameObject.class.getName());

	private GameObject parent;
	private StringProperty nameProperty = new SimpleStringProperty();

	private Comparator<Class<? extends Component>> byClassComparator = (o1, o2) -> {
		boolean result = o1.isAssignableFrom(o2) || o2.isAssignableFrom(o1);
		if (result) {
			return 0;
		}
		return o1.getName().compareTo(o2.getName());
	};

	private ObservableMap<Class<? extends Component>, Component> components = new ObservableMapWrapper<>(new TreeMap<>(byClassComparator));
	private ObservableMap<String, GameObject> children = FXCollections.observableMap(new TreeMap<>());

	public GameObject(String name) {
		nameProperty.set(name);
	}

	public GameObject(GameObject parent) {
		this("GameObject", parent);
	}

	public GameObject(String name, GameObject parent) {
		Objects.requireNonNull(parent, "Cannot createChild GameObject with parent set to null");
		this.parent = parent;
		parent.attach(this);
		nameProperty.set(name);
	}

	/**
	 * Adds the given component to the list of components of this. Destroys the previous component
	 * of the samve class if necessary.
	 * @param component
	 */
	public <T extends Component> GameObject attach(T component) {
		if (component.getOwner() != this) {
			component.detachFromOwner();
			component.ownerProperty.set(this);
			injectComponentFields(component);
			injectOtherFields(component);
			Component old = components.put(component.getClass(), component);
			if (old != null) {
				old.detachFromOwner();
			}
			log.info("Attached: " + component.getClass().getSimpleName());
		}
		return this;
	}

	private void injectComponentFields(Component component) {
		Field[] fields = component.getClass().getDeclaredFields();
		for (Field field : fields) {
			if (field.isAnnotationPresent(Inject.class)) {
				field.setAccessible(true);
				Class type = field.getType();
				Component instance = get(type);
				if (instance == null) {
					log.warning("Cannot find component: " + type.getSimpleName());
					continue;
				}
				try {
					field.set(component, instance);
				} catch (IllegalAccessException e) {
					log.warning(e.getLocalizedMessage());
				}
			}
		}
	}

	private void injectOtherFields(Component source) {
		for (Component component : components.values()) {
			for (Field field : component.getClass().getDeclaredFields()) {
				if (field.isAnnotationPresent(Inject.class) && field.getType().equals(source.getClass())) {
					try {
						field.setAccessible(true);
						field.set(component, source);
					} catch (IllegalAccessException e) {
						log.warning(e.getLocalizedMessage());
					}
				}
			}
		}
	}

	public <T extends Component> void detach(Class<T> componentClass) {
		T old = (T) components.remove(componentClass);
		if (old != null) {
			old.onDestroy();
			old.ownerProperty = null;
		}
	}

	private void nullifyInjections() {

	}

	/**
	 * Returns a component of the given class.
	 * @param componentClass
	 * @param <T>
	 * @return component of the given class or null if no component of the given class is attached
	 * to this.
	 */
	public <T extends Component> T get(Class<T> componentClass) {
		T found = (T) components.get(componentClass);
		return found;
	}

	/**
	 * Returns a collection of components attached to this gameobject
	 * @return collection of components
	 */
	public Collection<Component> getComponents() {
		return components.values();
	}

	/**
	 * Changes the child's parent to this. Detaches from other parents if necessary.
	 * @param child child which should be attached to this
	 */
	public void attach(GameObject child) {
		if (child.parent == this) {
			return;
		}
		child.detachFromParent();
		child.parent = this;
		String newName = uniqueName(child);
		children.put(newName, child);
		child.nameProperty.addListener(this::childRenamed);
	}

	/**
	 * Removes the given child from the list of this' children. Sets the parent to null.
	 * @param child
	 */
	private void detach(GameObject child) {
		children.remove(child.nameProperty.get());
		child.parent = null;
		child.nameProperty.removeListener(this::childRenamed);
	}

	private void detachFromParent() {
		if (parent != null) {
			parent.detach(this);
		}
	}

	public Collection<GameObject> getChildren() {
		return children.values();
	}

	private void childRenamed(Observable o, String oldName, String newName) {
		GameObject child = children.remove(oldName);
		children.put(newName, child);
	}

	private String uniqueName(GameObject child) {
		String oldName = child.nameProperty.get();
		String newName = oldName;
		int counter = 1;
		while (children.containsKey(newName)) {
			newName = oldName + " (" + counter + ")";
			counter += 1;
		}
		if (newName != oldName) {
			child.nameProperty.set(newName);
		}
		return newName;
	}

}
