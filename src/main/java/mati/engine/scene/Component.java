package mati.engine.scene;

import javafx.beans.Observable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import mati.engine.scene.component.annotation.Inject;

import java.lang.reflect.Field;
import java.util.Comparator;
import java.util.logging.Logger;

public abstract class Component {
	protected static final Logger log = Logger.getLogger(Component.class.getName());

	ObjectProperty<GameObject> ownerProperty = new SimpleObjectProperty<>();

	{
		ownerProperty.addListener(this::ownerChanged);
	}

	void detachFromOwner() {
		if (ownerProperty.get() != null) {
			ownerProperty.get().detach(getClass());
		}
	}

	private void ownerChanged(Observable observable, GameObject oldValue, GameObject newValue) {

	}

	public GameObject getOwner() {
		return ownerProperty.get();
	}

	public void onStart() {

	}

	public void onDestroy() {

	}
}
