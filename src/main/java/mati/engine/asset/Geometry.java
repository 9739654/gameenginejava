package mati.engine.asset;

import mati.engine.util.buffer.IndexBufferObject;
import mati.engine.util.buffer.Vector3fBufferObject;

public class Geometry {
	public int verticesPerFace;
	public boolean solid;
	public IndexBufferObject ibo;
	public Vector3fBufferObject vbo;
}
