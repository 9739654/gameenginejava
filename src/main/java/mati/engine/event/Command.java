package mati.engine.event;

@FunctionalInterface
public interface Command {
	void execute();
}
