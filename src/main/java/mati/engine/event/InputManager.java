package mati.engine.event;

import mati.engine.window.Window;
import org.lwjgl.glfw.GLFW;

public interface InputManager {

	void bind(Window window);

	void unbind();

	void poll();

	void setKeyCommand(KeyAction action, int key, Command command);

	Command getKeyCommand(KeyAction action, int key);

	void setCursorPosCommand(Command command);

	Command getCursorPosCommand();

	enum KeyAction {
		Press(GLFW.GLFW_PRESS), Release(GLFW.GLFW_RELEASE);

		private int code;

		KeyAction(int code) {
			this.code = code;
		}

		public int code() {
			return code;
		}
	};
}
