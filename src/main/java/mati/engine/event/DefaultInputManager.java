package mati.engine.event;

import mati.engine.scene.Component;
import mati.engine.window.Window;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;

import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

import static org.lwjgl.glfw.GLFW.*;

public class DefaultInputManager implements InputManager {
	protected static final Logger log = Logger.getLogger(DefaultInputManager.class.getName());

	private long windowHandle;
	private GLFWKeyCallback keyCallback;
	private GLFWCursorPosCallback cursorPosCallback;
	private GLFWMouseButtonCallback mouseButtonCallback;

	private Map<Integer, Command> keyPressCommands;
	private Map<Integer, Command> keyReleaseCommands;
	private KeyActionMap keyActionMap;

	private Map<Integer, Command> buttonPressCommands;
	private Map<Integer, Command> buttonReleaseCommands;
	private KeyActionMap buttonActionMap;

	private Command cursorPosCommand;

	{
		windowHandle = -1;
		keyPressCommands = new TreeMap<>();
		keyReleaseCommands = new TreeMap<>();
		keyActionMap = new KeyActionMap() {
			@Override
			public Map<Integer, Command> get(int action) {
					switch (action) {
						case GLFW.GLFW_PRESS:
							return DefaultInputManager.this.keyPressCommands;

						case GLFW.GLFW_RELEASE:
							return DefaultInputManager.this.keyReleaseCommands;

						default:
							return empty;
					}
			}
		};

		buttonPressCommands = new TreeMap<>();
		buttonReleaseCommands = new TreeMap<>();
		buttonActionMap = new KeyActionMap() {
			@Override
			public Map<Integer, Command> get(int action) {
					switch (action) {
						case GLFW.GLFW_PRESS:
							return DefaultInputManager.this.buttonPressCommands;

						case GLFW.GLFW_RELEASE:
							return DefaultInputManager.this.buttonReleaseCommands;

						default:
							return empty;
					}
			}
		};
	}

	private abstract class KeyActionMap {

		protected final Map<Integer, Command> empty = new TreeMap<Integer, Command>();

		public Map<Integer, Command> get(KeyAction action) {
			return get(action.code());
		}

		public abstract Map<Integer, Command> get(int action);
	}

	@Override
	public void bind(Window window) {
		if (windowHandle < 0) {
			windowHandle = window.getHandle();
		} else {
			log.warning("trying to set window handle ");
		}
		createKeyCallback();
		createCursorPosCallback();
		createMouseButtonCallback();
	}

	private void createKeyCallback() {
		if (keyCallback == null) {
			keyCallback = new GLFWKeyCallback() {
				@Override
				public void invoke(long window, int key, int scancode, int action, int mods) {
					if (key != GLFW.GLFW_KEY_UNKNOWN) {
						Command command;
						Map<Integer, Command> keyCommands = keyActionMap.get(action);
						command = keyCommands.get(key);
						if (command != null) {
							command.execute();
						}
					}
				}
			};
			glfwSetKeyCallback(windowHandle, keyCallback);
		} else {
			log.warning("trying to create key callback for window " + windowHandle + " again.");
		}
	}

	private void createCursorPosCallback() {
		if (cursorPosCallback == null) {
			cursorPosCallback = new GLFWCursorPosCallback() {
				@Override
				public void invoke(long window, double xpos, double ypos) {
					if (cursorPosCommand != null) {
						cursorPosCommand.execute();
					}
				}
			};
			glfwSetCursorPosCallback(windowHandle, cursorPosCallback);
		} else {
			log.warning("trying to create cursor pos callback for window " + windowHandle + " again.");
		}
	}

	private void createMouseButtonCallback() {
		if (mouseButtonCallback == null) {
			mouseButtonCallback = new GLFWMouseButtonCallback() {
				@Override
				public void invoke(long window, int button, int action, int mods) {
					Command command = buttonActionMap.get(action).get(button);
					if (command != null) {
						command.execute();
					}
				}
			};
			glfwSetMouseButtonCallback(windowHandle, mouseButtonCallback);
		} else {
			log.warning("trying to create mouse button callback for window " + windowHandle + " agaain.");
		}
	}

	@Override
	public void unbind() {
		releaseKeyCallback();
		releaseCursorPosCallback();
	}

	private void releaseKeyCallback() {
		if (keyCallback != null) {
			keyCallback.release();
			keyCallback = null;
		} else {
			log.warning("trying again to release key callback from window " + windowHandle);
		}
	}

	private void releaseCursorPosCallback() {
		if (cursorPosCallback != null) {
			cursorPosCallback.release();
			cursorPosCallback = null;
		} else {
			log.warning("trying again to release cursor pos callback from window " + windowHandle);
		}
	}

	@Override
	public void poll() {
		glfwPollEvents();
	}

	@Override
	public void setKeyCommand(KeyAction action, int key, Command command) {
		keyActionMap.get(action).put(key, command);
	}

	@Override
	public Command getKeyCommand(KeyAction action, int key) {
		return keyActionMap.get(action).get(key);
	}

	@Override
	public void setCursorPosCommand(Command command) {
		this.cursorPosCommand = command;
	}

	@Override
	public Command getCursorPosCommand() {
		return cursorPosCommand;
	}


}
