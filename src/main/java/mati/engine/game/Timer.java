package mati.engine.game;

/**
 * @author Mateusz Paluch
 */
public interface Timer {
	public double now();
}
