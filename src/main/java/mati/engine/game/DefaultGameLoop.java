package mati.engine.game;

import java.util.concurrent.atomic.AtomicBoolean;

public class DefaultGameLoop implements GameLoop {

	private AtomicBoolean running;
	private Timer timer;
	private Action initAction;
	private Action inputAction;
	private Action logicAction;
	private Action drawAction;
	private Action finalizeAction;

	{
		running = new AtomicBoolean(false);
	}

	@Override
	public void setTimer(Timer timer) {
		this.timer = timer;
	}

	@Override
	public void setInitAction(Action action) {
		this.initAction = action;
	}

	@Override
	public void setInputAction(Action action) {
		this.inputAction = action;
	}

	@Override
	public void setLogicAction(Action action) {
		this.logicAction = action;
	}

	@Override
	public void setDrawAction(Action action) {
		this.drawAction = action;
	}

	@Override
	public void setFinalizeAction(Action action) {
		this.finalizeAction = action;
	}

	@Override
	public void start() {
		running.set(true);
		initAction.execute();
		while (!running.get()) {
			inputAction.execute();
			logicAction.execute();
			drawAction.execute();
		}
		finalizeAction.execute();
	}

	@Override
	public void stop() {
		running.set(false);
	}
}
