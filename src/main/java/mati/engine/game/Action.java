package mati.engine.game;

/**
 * @author Mateusz Paluch
 */
public interface Action {
	void execute();
}
