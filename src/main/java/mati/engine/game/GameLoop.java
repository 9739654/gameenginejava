package mati.engine.game;

/**
 * @author Mateusz Paluch
 */
public interface GameLoop {
	void setTimer(Timer timer);
	void setInitAction(Action action);
	void setInputAction(Action action);
	void setLogicAction(Action action);
	void setDrawAction(Action action);
	void setFinalizeAction(Action action);
	void start();
	void stop();
}
