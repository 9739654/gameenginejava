package mati.engine.game;

import mati.engine.event.InputManager;
import mati.engine.render.RenderManager;
import mati.engine.scene.Scene;
import mati.engine.window.Window;

/**
 * @author Mateusz Paluch
 */
public interface Game {
	void set(Window window);
	void set(InputManager manager);
	void set(RenderManager manager);
	void set(Scene scene);

	void start();
}
