package mati.engine.game;

import mati.engine.event.InputManager;
import mati.engine.render.RenderManager;
import mati.engine.scene.GameObject;
import mati.engine.scene.Scene;
import mati.engine.util.Color;
import mati.engine.window.Window;
import org.lwjgl.glfw.GLFW;

/**
 * @author Mateusz Paluch
 */
public class DefaultGame implements Game {

	private Window window;
	private InputManager inputManager;
	private RenderManager renderManager;
	private Scene scene;

	@Override
	public void set(Window window) {
		this.window = window;
	}

	@Override
	public void set(InputManager manager) {
		this.inputManager = manager;
	}

	@Override
	public void set(RenderManager manager) {
		this.renderManager = manager;
	}

	@Override
	public void set(Scene object) {
		this.scene = scene;
	}

	@Override
	public void start() {
		window.init();
		window.show();

		inputManager.setKeyCommand(InputManager.KeyAction.Press, GLFW.GLFW_KEY_ESCAPE, () -> {
			System.out.println("ESC was pressed");
			window.close();
		});
		inputManager.bind(window);

		renderManager.init();
		renderManager.setBackground(new Color(.2f, .2f, .5f));
		renderManager.setScene(scene);

		while (!window.isClosing()) {
			inputManager.poll();

			renderManager.draw();
			window.swapBuffers();
		}
	}
}
