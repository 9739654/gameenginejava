package mati.engine.render;

import mati.engine.scene.Scene;
import mati.engine.util.Color;
import mati.engine.util.Size;
import mati.engine.window.Window;

public interface RenderManager {
	void init();
	void setBackground(Color color);
	void setFrameSize(Size newSize);

	void bind(Window window);

	void setScene(Scene scene);

	void draw();
}
