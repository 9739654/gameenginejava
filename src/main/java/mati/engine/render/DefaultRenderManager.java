package mati.engine.render;

import mati.engine.scene.GameObject;
import mati.engine.scene.Scene;
import mati.engine.util.Color;
import mati.engine.util.Size;
import mati.engine.window.Window;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GLCapabilities;

import java.util.ArrayDeque;
import java.util.Collection;

import static org.lwjgl.opengl.GL11.*;

public class DefaultRenderManager implements RenderManager {

	private GLCapabilities capabilities = null;
	private Color backgroundColor = new Color();
	private Size frameSize;
	private Scene scene;

	private ArrayDeque renderQueue = new ArrayDeque();


	@Override
	public void init() {
		capabilities = GL.createCapabilities();
		updateBackgroundColor();
	}

	@Override
	public void setBackground(Color color) {
		this.backgroundColor = color;
		if (capabilities != null) {
			updateBackgroundColor();
		}
	}

	@Override
	public void setFrameSize(Size newSize) {
		this.frameSize = newSize;
		glViewport(0, 0, frameSize.getWidth(), frameSize.getHeight());
	}

	@Override
	public void bind(Window window) {
		window.addFrameSizeChangeListener(this::setFrameSize);
	}

	@Override
	public void setScene(Scene scene) {
		this.scene = scene;
	}

	private void updateBackgroundColor() {
		glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, 0);
	}

	@Override
	public void draw() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	}

	public void drawChild(GameObject go) {

	}
}
