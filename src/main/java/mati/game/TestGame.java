package mati.game;

import mati.engine.event.DefaultInputManager;
import mati.engine.event.InputManager;
import mati.engine.game.DefaultGame;
import mati.engine.game.Game;
import mati.engine.render.DefaultRenderManager;
import mati.engine.render.RenderManager;
import mati.engine.scene.Scene;
import mati.engine.window.Window;

/**
 * @author Mateusz Paluch
 */
public class TestGame {
	public static void main(String[] args) {
		new TestGame().start();
	}

	private void start() {
		Window window = new Window();
		InputManager input = new DefaultInputManager();
		RenderManager render = new DefaultRenderManager();
		Scene scene = new Scene();

		Game g = new DefaultGame();
		g.set(window);
		g.set(input);
		g.set(render);
		g.set(scene);
		g.start();
	}
}
