import mati.engine.scene.Component;
import mati.engine.scene.GameObject;
import mati.engine.scene.Scene;
import mati.engine.scene.component.DisplayMaterial;
import mati.engine.scene.component.DisplayMesh;
import mati.engine.scene.component.RenderSettings;
import mati.engine.scene.component.annotation.Inject;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ComponentTest {

	@Test
	public void componentCanInjectSubclassFields() {
		// given
		GameObject go = new GameObject("root");
		Component material = new DisplayMaterial();
		Component mesh = new DisplayMesh();
		RenderSettings rs = new RenderSettings();
		go.attach(material).attach(mesh);

		// when
		go.attach(rs);

		// then
		assertNotNull(rs.getMaterial());
		assertNotNull(rs.getMesh());
	}

	@Test
	public void injectionOfTwoDirectionDependencies() {
		// given
		GameObject go = new GameObject("root");
		Component material = new DisplayMaterial() {
			@Inject
			RenderSettings renderSettings;
		};
		Component mesh = new DisplayMesh();
		RenderSettings rs = new RenderSettings();
		go.attach(material).attach(mesh);

		// when
		go.attach(rs);

		// then
		assertNotNull(rs.getMaterial());
		assertNotNull(rs.getMesh());

		Component value = null;
		try {
			Field field = material.getClass().getDeclaredField("renderSettings");
			value = (Component) field.get(material);
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		assertNotNull(value);
	}

}
