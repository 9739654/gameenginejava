package mati.engine.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.joml.Vector3f;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class FunctionalJsonTest {

	private static void check(boolean check) {
		assertTrue(check);
	}

	private static void checkNot(boolean check) {
		assertFalse(check);
	}

	@Test
	public void testHas() throws Exception {
		JsonObject object = new JsonObject();
		object.addProperty("property1", "value1");
		object.addProperty("prop2", 2);

		boolean test1 = FunctionalJson.has(object, "property1");
		boolean test2 = FunctionalJson.has(object, "prop2");
		boolean test3 = FunctionalJson.has(object, "other");

		assertTrue(test1);
		assertTrue(test2);
		assertFalse(test3);
	}

	@Test
	public void testHasAny() throws Exception {
		JsonObject object = new JsonObject();
		object.addProperty("prop1", "val1");
		object.addProperty("prop2", "val2");

		boolean test1 = FunctionalJson.hasAny(object, "prop1");
		boolean test2 = FunctionalJson.hasAny(object, "prop2");
		boolean test3 = FunctionalJson.hasAny(object, "other");

		assertTrue(test1);
		assertTrue(test2);
		assertFalse(test3);
	}

	@Test
	public void testHasAll() throws Exception {
		JsonObject o = new JsonObject();
		o.addProperty("p1", "v1");
		o.addProperty("p2", "v2");

		boolean test1 = FunctionalJson.hasAll(o, "p1", "p2");
		boolean test2 = FunctionalJson.hasAll(o, "p1");
		boolean test3 = FunctionalJson.hasAll(o, "p1", "p2", "p3");
		boolean test4 = FunctionalJson.hasAll(o, "p3");

		check(test1);
		check(test2);
		checkNot(test3);
		checkNot(test4);
	}

	@Test
	public void testDoWith() throws Exception {

	}

	@Test
	public void testElement() throws Exception {
		JsonObject o = new JsonObject();
		o.addProperty("p1", "v1");
		o.addProperty("p2", "v2");

		JsonElement element = FunctionalJson.element(o, "p1");

		assertNotNull(element);
	}

	@Test
	public void testObject() throws Exception {

	}

	@Test
	public void testString() throws Exception {

	}

	@Test
	public void testType() throws Exception {

	}

	@Test
	public void testType1() throws Exception {

	}

	@Test
	public void canParseArrayFromJsonArray() {
		JsonObject obj = new JsonObject();
		JsonArray jsonArray = new JsonArray();
		jsonArray.add("x");
		jsonArray.add("y");
		jsonArray.add("z");
		obj.add("fields", jsonArray);

		JsonArray array = FunctionalJson.array(obj, "fields");
		ArrayList<String> strings = FunctionalJson.strings(array);
		Field[] fields = FunctionalJson.Ext.fields(strings, FunctionalJson.Ext::declaredField, Vector3f.class);

	}

	@Test
	public void canParseArrayFromJsonObject() {

	}

	@Test
	public void testArray2() throws Exception {

	}

	@Test
	public void testStrings() throws Exception {

	}

	@Test
	public void testStrings1() throws Exception {

	}

	@Test
	public void testStrings2() throws Exception {

	}
}