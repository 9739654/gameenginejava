package mati.engine.util;

import com.google.gson.*;
import mati.engine.scene.component.DisplayMaterial;
import org.junit.Test;

import java.io.*;
import java.net.URL;
import java.util.List;

import static org.junit.Assert.*;

public class DisplayMaterialFactoryJsonTest {

	@Test
	public void canLoadFromUrl() {
		URL url = getClass().getResource("/mati/engine/util/test.material.json");
		DisplayMaterialFactory factory = new DisplayMaterialFactory();
		List<DisplayMaterial> materials = factory.load(url);

		//TODO test
	}

	@Test
	public void canLoadFromJsonObject() throws IOException {
		URL url = getClass().getResource("/mati/engine/util/test.material.json");
		JsonParser parser = new JsonParser();
		InputStreamReader isr = new InputStreamReader(url.openStream());
		JsonElement element = parser.parse(isr);
		JsonObject object = element.getAsJsonArray().get(0).getAsJsonObject();

		DisplayMaterialFactory factory = new DisplayMaterialFactory();
		DisplayMaterial material = factory.from(object);

		//TODO test
		assertEquals("Example material", material.name);
		assertEquals(2, material.extractMappings.size());
		assertEquals(3, material.valueMappings.size());
	}
}